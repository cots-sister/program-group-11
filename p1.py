import socket
import time

def cekmenang(b):
	#cek apakah ada yang menang
	#jika X menang, yang di return adalah X
	#jika O menang, O di return
	if b[0] is not " ": #jika 0 tidak kosong, maka akan cek kemungkinan menang dari indeks 0
		if b[0]==b[1] and b[0]==b[2]:
			return b[0]
		elif b[0]==b[4] and b[0]==b[8]:
			return b[0]
		elif b[0]==b[3] and b[0]==b[6]:
			return b[0]

	if b[1] is not " ":
		if b[1]==b[4] and b[1]==b[7]:
			return b[1]
	
	if b[2] is not " ":
		if b[2]==b[4] and b[2]==b[6]:
			return b[2]
		elif b[2]==b[5] and b[2]==b[8]: 
			return b[2]

	if b[3] is not " ":
		if b[3]==b[4] and b[3]==b[5]:
			return b[3]
	
	if b[6] is not " ":
		if b[6]==b[7] and b[6]==b[8]: 
			return b[6]

	if " " not in b: #mengecek apakah masih ada kosong dalam list
		return "BOARD PENUH"


def judge(b, gilir):
	selesai = False
	#cek apakah ada yang sudah menang
	pemenang = cekmenang(b)
	if pemenang: #jika ada yang menang(jika variable pemenang ada isinya, kalau tidak ada yang menang, variable pemenang tidak ada isi nya)
		if pemenang == 'X':
			print("Selamat!! Anda MENANG!!!!")
		elif pemenang == 'O':
			p2.send("Selamat!! Anda MENANG!!!!".encode())
			time.sleep(0.5)
		else:
			pass
		#mengubah isi variable menjadi 2, yang berarti selesai
		gilir = int(2)
		selesai = True
	kirimstate(b, gilir)
	return selesai


def kirimstate(b, gilir):
	for i in range(0,7,3):
		#loop board, dari 0 sampai 7, dan setiap iterasi i nya + 3
		p2.send(("|" + board[i] + "|" + board[i+1] + "|" + board[i+2] + "|").encode())
		time.sleep(0.5)
	p2.send("-------------".encode())
	time.sleep(1)
	#mengirim giliran dan 'stop' misal: 0stop berarti giliran 0
	p2.send((repr(gilir)+'stop').encode())
	time.sleep(0.5)



ip = 'localhost'

port = 12345

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((ip, port))
s.listen(1)
p2, addr = s.accept()

#inisiasi giliran 0, yaitu giliran P1
giliran = int(0)
#membuat board dengan panjang 9 dan tiap elemen isinya " "
board = [" "] * 9

loop = True

while loop:
	if giliran==0: #jika giliran P1
		posisi = int(input("Masukkan posisi X : "))
		if posisi >= 0 and posisi < 9: #cek apakah inputtan diantara 0-8
			if board[posisi] == " ": #jika board di index posisi tidak ada isinya
				board[posisi] = 'X'
				giliran = int(1)
				if judge(board, giliran): #jika fungsi juge me-return True
					loop = False #maka loop dihentikan, permainan berhenti
			else:
				print("Posisi sudah terisi")
		else:
			print("Masukkan angka 0 - 8")

	else:
		posisi = int(p2.recv(32).decode())
		if posisi >= 0 and posisi < 9:
			if board[posisi] == " ":
				board[posisi] = 'O'
				giliran = int(0)
				if judge(board, giliran):
					loop = False
			else:	
				p2.send(("Posisi sudah terisi").encode())
				time.sleep(0.2)
				p2.send((repr(giliran)+'stop').encode())
				time.sleep(0.2)
		else:
			p2.send(("Masukkan angka 0 - 8").encode())
			time.sleep(0.2)
			p2.send((repr(giliran)+'stop').encode())
			time.sleep(0.2)

	for i in range(0,7,3):
		#print board saat ini, iterasi 0 sampai 7
		print(("|" + board[i] + "|" + board[i+1] + "|" + board[i+2] + "|"))
	print()

p2.close()
