# Program Group 11

## Deskripsi Program
Program ini dibuat menggunakan konsep interprocess communication menggunakan socket programming. Bahasa yang digunakan dalam program ini adalah bahasa pemrograman python.
Input pada program tiap gilirannya adalah berupa angka indeks dari posisi kotak yang ingin dimasukkan (Sesuai dengan gambar dibawah).
Inputan pilih posisi yang pertama diberikan kepada server kemudian setelah diisi oleh orang pertama maka akan di receive oleh orang kedua/client, lakukan hal yang sama 
sampai program telah selesai dan akan menampilkan pemenangnya dan jika hasilnya imbang maka akan muncul permainan imbang.

![TicTacToe](img/tictac.png)

## Cara menjalankan file program
Ada beberapa yang dibutuhkan sebelum membuat dan menjalankan program ini, antara lain :
1. OS : Linux
2. Bahasa Pemrograman : Python
3. Library : Socket

Setelah itu langkah selanjutnya :
1. Jalankan p1.py (server) dan p2.py (client) pada terminal
2. ![langkah1](img/langkah1.png)
Setelah dijalankan, pada bagian ini server diminta untuk melakukan inputan 
3. ![langkah2](img/langkah2.png) 
Setelah itu giliran lawan/client untuk melakukan inputan pilih posisi
4. ![langkah3](img/langkah3.png) 
Setelah itu lakukan hal yang sama seperti langkah 2 hingga terdapat pemenang
5. ![langkah4](img/langkah4.png) 
Lakukan hal yang sama seperti langkah 2
6. ![langkah5](img/langkah5.png) 
Jika sudah ada pemenang maka game akan berakhir


## Pembagian Tugas
### 1. Hilman Abdan
        - Membuat p1.py (server)
        - Membuat dokumentasi program

### 2. Haris Hamdani Latif
       - Membuat p2.py (client)
       - Membuat dokumentasi program






